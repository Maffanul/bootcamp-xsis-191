package com.xsis.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.Mahasiswa;

@Controller
public class MhasiswaController {

	@RequestMapping("/mahasiswa/input")
	public String input() {
		return "mahasiswa/input";
	}
	
	@RequestMapping(value="/mahasiswa/save", method=RequestMethod.POST)
	public String save(@ModelAttribute Mahasiswa item, Model model) {
		model.addAttribute("data",item);
		return "mahasiswa/save";
	}
}
