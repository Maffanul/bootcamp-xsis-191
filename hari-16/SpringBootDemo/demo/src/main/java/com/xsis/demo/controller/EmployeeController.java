package com.xsis.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.annotation.JacksonInject.Value;
import com.xsis.demo.model.Employee;
import com.xsis.demo.repository.EmployeeRepo;

@Controller
public class EmployeeController {
	// membuat auto instance dari repository
	@Autowired
	private EmployeeRepo repo;
	
	//req yang ada di url localhost:port/biodata/index
	@RequestMapping("/employee/index")
	public String index(Model model) {
		
		//membuat object list biodata
		//kemudian diisi dari object repo dengan method find all
		List<Employee> data =repo.findAll();
		//mengirim variable listData, valuenya diisi dari object data
		model.addAttribute("listData", data);
		// menampilkan view /src/main/resource/templates
		return "/employee/index";
	}
	
	@RequestMapping("/employee/add")
	public String add() {
		
		return "/employee/add";
	}
	
	@RequestMapping(value="/employee/save", method=RequestMethod.POST)
	public String save(@ModelAttribute Employee item) {
		repo.save(item);
		
		return"redirect:/employee";
	}
	
	//request edit data
	@RequestMapping(value="/employee/edit/{id}")
	public String edit(Model model,@PathVariable(name="id") Long id) {
		Employee item= repo.findById(id).orElse(null);
		model.addAttribute("data", item);
		return "employee/edit";
	}
	
	
	//request delete data
	@RequestMapping(value="/employee/delete/{id}")
	public String hapus(@PathVariable(name="id") Long id) {
		//mengambil data dari database dengan parameter id
		Employee item = repo.findById(id).orElse(null);
		// remove from data base
		if(item != null) {
			repo.delete(item);
		}return  "redirect:/employee";
	}
}
