package hackerrank.java;

import java.util.Scanner;

public class JavaDatatypes4 {
	static Scanner scss;
	
	public static void main(String[] args) {
	
		scss = new Scanner(System.in);
		int w = scss.nextInt();
		
		for (int l = 0; l < w; l++) {
			try {
				long b = scss.nextLong();
				System.out.println(b+"can be fitted in: ");
				if(b>=-128 && b<=127)
					System.out.println("* byte");
				if(b>= (Math.pow(2, 15)*-1) && b<= Math.pow(2, 15)-1)
					System.out.println("* short");
				if(b>= (Math.pow(2, 31)*-1) && b<= Math.pow(2, 31)-1)
					System.out.println("* int");
				if(b>= (Math.pow(2, 36)*-1) && b<= Math.pow(2, 63)-1)
					System.out.println("* long");
				
				
			} catch (Exception e) {
				System.out.println(scss.next()+"can't be fitted anywere.");
				// TODO: handle exception
			}
			
		}
	}
}
