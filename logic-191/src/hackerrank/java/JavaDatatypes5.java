package hackerrank.java;

import java.util.Scanner;

public class JavaDatatypes5 {
	static Scanner nsc;
	
	public static void main(String[] args) {
		nsc = new Scanner (System.in);
		int x = nsc.nextInt();
		
		for (int m = 0; m < x; m++) {
			try {
			
				long z = nsc.nextLong();
				System.out.println(z+"can be fitted in : ");
				if(z>=-128 && z<=127)
					System.out.println("* byte");
				if(z>= (Math.pow(2, 15)*-1) && z<= Math.pow(2, 15)-1)
					System.out.println("* short");
				if(z>= (Math.pow(2, 31)*-1) && z<= Math.pow(2, 31)-1)
					System.out.println("* int");
				if(z>= (Math.pow(2, 63)*-1)&& z<=Math.pow(2, 63)-1)
					System.out.println("* long");
				
			} catch (Exception e) {
				System.out.println(nsc.next()+"can't be fitted anyewre. ");
			}
			
		}
	}
}
