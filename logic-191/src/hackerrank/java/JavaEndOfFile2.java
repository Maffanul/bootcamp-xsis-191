package hackerrank.java;

import java.util.Scanner;

public class JavaEndOfFile2 {
	static Scanner nsd;
	
	public static void main(String[] args) {
		nsd = new Scanner(System.in);
		
		int j = 1;
		while(nsd.hasNext()) {
			System.out.println(j+" "+nsd.nextLine());
			j++;
		}
	}

}
