package hackerrank.java;

import java.util.Scanner;

public class JavaStaticInitializerBlock2 {
	static int A;
	static int B;
	static boolean flag = false;
	static final Scanner scnn = new Scanner(System.in);
			
	static {
		A = scnn.nextInt();
		B =scnn.nextInt();
		if(A>0 && B>0) {
			flag = true;
		}else {
			System.out.println("java.lang.Exception: Breadth and hight must be positive ");
		}
		
	}
	
	public static void main(String[] args) {
		if(flag) {
			int arr = A*B;
			System.out.println(arr);
		}
	}
}
