package hackerrank.java;

import java.util.Scanner;

public class JavaStaticInitializerBlock5 {
	static int G;
	static int H;
	static boolean flag = true;
	
	static final Scanner ade = new Scanner(System.in);
	static {
		G=ade.nextInt();
		H=ade.nextInt();
		if(G>0 && H>0) {
			flag = true;
		}else {
			System.out.println("java.lang.Exception: Breadth adn hight mus be positive ");
		}
	}
	public static void main(String[] args) {
		if(flag) {
			int ara = G*H;
			System.out.println(ara);
		}
	}
}
