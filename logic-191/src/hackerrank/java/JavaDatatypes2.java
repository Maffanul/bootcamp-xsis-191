package hackerrank.java;

import java.util.Scanner;

public class JavaDatatypes2 {
	static Scanner nscn;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		nscn = new Scanner(System.in);
		int u = nscn.nextInt();
		
		for (int i = 0; i < u; i++) {
			try {
				long a=nscn.nextLong();
				System.out.println(a+"can be fitted in: ");
				if(a>=-128 && a<=127)
					System.out.println("* Byte");
				if(a>= (Math.pow(2, 15)*-1) && a<= Math.pow(2, 15)-1)
					System.out.println("* short");
				if(a>= (Math.pow(2, 31)*-1) && a<= Math.pow(2, 31)-1)
					System.out.println("* int");
				if(a>= (Math.pow(2, 63)*-1) && a<=Math.pow(2, 63)-1)
					System.out.println("* long");
			}
			catch (Exception e) {
				// TODO: handle exception
				System.out.println(nscn.next()+"can'y be fitted anywere. ");
			}
		}
	}

}
