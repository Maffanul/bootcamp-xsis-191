package hackerrank.java;

import java.util.Scanner;

public class IfElse {
	static Scanner scan;
	public static void main(String[] args) {
		scan=new Scanner(System.in);
		int N =scan.nextInt();
		
		
		if(N%2==1) {
			System.out.println("Weird");
		}else {
			if(N>=2 && N<=5) {
				System.out.println("Not Weird");
			}else if(N>=6 && N<=20) {
				System.out.println("Weird");
			}else {
				System.out.println("Not Weird");
			}
		}
	}

}
