package hackerrank.java;

import java.util.Scanner;

public class IfElse5 {
	static Scanner inn;
	
	public static void main(String[] args) {
		inn = new Scanner(System.in);
		
		int K = inn.nextInt();
		
		if(K%2==1) {
			System.out.println("Werid");
		}else {
			if(K>=2 && K<=5) {
				System.out.println("Not Werid");
			}else if(K>=6 && K<=20) {
				System.out.println("Werid");
			}else {
				System.out.println("Not Werid");
			}
		}
	}

}
