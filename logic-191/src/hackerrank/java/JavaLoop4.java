package hackerrank.java;

import java.util.Scanner;

public class JavaLoop4 {

	static Scanner sccn;
	public static void main(String[] args) {
		//instansiasi dari calss Scanner
		sccn = new Scanner(System.in);		
		//input 
		int P = sccn.nextInt();
		for (int a = 1; a <=10; a++) {
			int perkalian = P*a;
			//output
			System.out.println(P+" x "+a+" = "+perkalian);
		}
	}
}
