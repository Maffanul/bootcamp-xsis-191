package hackerrank.java;

import java.util.Scanner;

public class JavaOutputFormating4 {
	
	public static void main(String[] args) {
		//instansiasi
		Scanner scsc =new Scanner(System.in);
		System.out.println("================");
		//input
		for (int j = 0; j < 3; j++) {
			String s2 = scsc.nextLine();
			int x2 = scsc.nextInt();
			
			//output
			System.out.printf("-15%s%s %n", s2, String.format("03d", x2));
		}
		System.out.println("=================");
	}
}
