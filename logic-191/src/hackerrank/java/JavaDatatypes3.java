package hackerrank.java;

import java.util.Scanner;

public class JavaDatatypes3 {
	static Scanner scnn;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		scnn = new Scanner(System.in);
		int v = scnn.nextInt();
		
		for (int k = 0; k < v; k++) {
			
			try {
				long y =scnn.nextLong();
				System.out.println(y+"can be fitted in: ");
				if(y>=-128 && y<=127)
					System.out.println("* byte");
				if(y>= (Math.pow(2, 15)*-1) && y<= Math.pow(2, 15)-1)
					System.out.println("* shot");
				if(y>= (Math.pow(2, 31)*-1) && y<= Math.pow(2, 31)-1)
					System.out.println("* int");
				if(y>= (Math.pow(2, 63)*-1) && y<= Math.pow(2, 63)-1)
					System.out.println("* long");
				
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(scnn.next()+"can't be fitted anywere");
			}
			
		}
	}

}
