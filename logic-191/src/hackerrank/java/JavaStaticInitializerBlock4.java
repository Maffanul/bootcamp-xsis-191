package hackerrank.java;

import java.util.Scanner;

public class JavaStaticInitializerBlock4 {

	static int E;
	static int F;
	static boolean flag = false;
	static final Scanner ase = new Scanner(System.in);
	
	static {
		E=ase.nextInt();
		F = ase.nextInt();
		if(E>0 && F>0) {
			flag = true;
		}else {
			System.out.println("java.lang.Exception: Breadth and hight mus be positive ");
		}
	}
	
	public static void main(String[] args) {
		if (flag) {
			int aer = E*F;
			System.out.println(aer);
		}
	}
}
