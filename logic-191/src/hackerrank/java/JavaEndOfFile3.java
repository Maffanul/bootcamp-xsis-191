package hackerrank.java;

import java.util.Scanner;

public class JavaEndOfFile3 {
	static Scanner abc;
	
	public static void main(String[] args) {
		abc = new Scanner(System.in);
		int k = 1;
		
		while(abc.hasNext()) {
			System.out.println(k+" "+abc.nextLine());
			k++;
		}
	}
}
