package hackerrank.java;

import java.util.Scanner;

public class JavaStaticInitializerBlock3 {
	static int C;
	static int D;
	static boolean flag=false;
	static final Scanner asc = new Scanner(System.in);
	
	static {
		C = asc.nextInt();
		D = asc.nextInt();
		if (C>0 && D>0) {
			flag = true;
			
		}else {
			System.out.println("java.lang.Exception: Breadth and hight mus be positive ");
		}
	}
	public static void main(String[] args) {
		if (flag) {
			int are = C*D;
			System.out.println(are);
		}
	}
	
}
