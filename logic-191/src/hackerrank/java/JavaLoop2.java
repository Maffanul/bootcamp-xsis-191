package hackerrank.java;

import java.util.Scanner;

public class JavaLoop2 {

	static Scanner scn;
	public static void main(String[] args) {
		//instansiasi
		scn = new Scanner(System.in);
		//input
		int M =scn.nextInt();
		for (int k = 1; k <=10; k++) {
			int kali = M*k;
			//output
			System.out.println(M+" x "+k+" = "+kali);
		}
	}
}
