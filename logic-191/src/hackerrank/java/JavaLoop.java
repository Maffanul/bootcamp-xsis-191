package hackerrank.java;

import java.util.Scanner;

public class JavaLoop {
	static Scanner scan;
	
	public static void main(String[] args) {
		//instansiasi scanner
		scan = new Scanner(System.in);
		int N =scan.nextInt();
		for (int i = 1; i <=10; i++) {
			int result = N*i;
			
			//output
			System.out.println(N+" x "+i+" = "+result);
		}
	}
}
