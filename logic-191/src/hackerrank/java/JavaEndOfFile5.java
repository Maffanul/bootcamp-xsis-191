package hackerrank.java;

import java.util.Scanner;

public class JavaEndOfFile5 {
	static Scanner qwe;
	
	public static void main(String[] args) {
		qwe = new Scanner (System.in);
		
		int m = 1;
		while(qwe.hasNext()) {
			System.out.println(m+" "+qwe.nextLine());
			m++;
		}
	}
}
