package hackerrank.java;

import java.util.Scanner;

public class JavaEndOfFile {
	static Scanner adc;
	
	public static void main(String[] args) {
		adc = new Scanner(System.in);
		int i = 1;
		while(adc.hasNext()) {
			System.out.println(i+" " +adc.nextLine());
			i++;
		}
	}
}
