package hackerrank.implementation;

public class GradingStudents5 {
	static int [] gradingStudent5(int[] grades) {
		for (int b = 0; b < grades.length; b++) {
			if(grades[b]<=38) {
				if(grades[b]+(5-grades[b]%5)-grades[b]<3) {
					grades[b] =(grades[b]+(5-grades[b]%5));
				}
			}
		}
		return grades;
	}
	public static void main(String[] args) {
		int [] b = {73,57,33,89};
		int [] o = gradingStudent5(b);
		System.out.println(o[0]);
		System.out.println(o[1]);
		System.out.println(o[2]);
		System.out.println(o[3]);
	}

}
