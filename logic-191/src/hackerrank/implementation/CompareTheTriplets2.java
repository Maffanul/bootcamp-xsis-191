package hackerrank.implementation;

import java.util.ArrayList;
import java.util.List;
//tentang perbandiangan nilai 2 anak, jika 1 anak nilai
// ada yang lebih besar, maka akan dapat poin 1
// jika nilai sama maka tidak ada yang mendapat nilai
public class CompareTheTriplets2 {
	static List<Integer> compare (List<Integer>a, List<Integer>b){ // membuat method untuk mengelist a dan b dengan type data integer , array list di gunakna
																	// karena array list bersifat dinamis dalam arti tidak perlu di inisialkan index nya berapa
		List<Integer> result = new ArrayList<Integer>(); // membuat variabel result dengan type data List yang di gunakan untuk menampung data 
		
		result.add(0); // untuk menampung data dari variabel result, index 0
		result.add(0); // untuk menampung  data dari cariabel result, index 1
		
		int nA=0; // untuk menampung data dari pengambilan nilai/ value
		int nB=0; // untuk menampung data dari pengambilan nilai/ value
		
		for (int i = 0; i < a.size(); i++) { // perulangan untuk mengakses atau memeriksa isi dari a
			if(a.get(i)>b.get(i)) { // kondisi dimana jika a [i] lebih besar dari pada b[i] atau menemukan
									// niali besaran a dari b dengan perbandinagn
				nA++;				// maka nA akan di tambah ke variabel method
				result.set(0, nA); // result ditambah dengan index ke 0  dengan nilai dari nA;
			}
			if(a.get(i)<b.get(i)) { // kondisi untuk mendapatkan niali dari besaran b 
									// dengan b lebih besar dari a maka variabel nB akan tambah
				nB++;				// variabel tambah nB
				result.set(1, nB);	// maka result akan menge set pada index ke 0 dan nilai nB
			}
		}
		return result; // pengembalian nilai dri method return berupa result
	}
	
	public static void main(String[] args) {
		List<Integer> a =new ArrayList<Integer>();
		a.add(12);
		a.add(11);
		a.add(11);
		
		List<Integer> b=new ArrayList<Integer>();
		b.add(13);
		b.add(11);
		b.add(10);
		
		for( Integer hasil : compare(a, b)) {
			System.out.println(hasil);
		}
	}
}
