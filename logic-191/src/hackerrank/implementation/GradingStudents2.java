package hackerrank.implementation;

public class GradingStudents2 {
	static int [] gradingStudent2 (int [] grades) {
		for (int j = 0; j < grades.length; j++) {
			if(grades[j]>=38) {
				if(grades[j]+(5-grades[j]%5)-grades[j]<3) {
					grades[j]=(grades[j]+(5-grades[j]%5));
				}
			}
		}
		return grades;
	}
	
	public static void main(String[] args) {
		int [] j = {37,68,32, 76};
		int [] h = gradingStudent2(j);
		System.out.println(h[0]);
		System.out.println(h[1]);
		System.out.println(h[2]);
		System.out.println(h[3]);
	}
}
