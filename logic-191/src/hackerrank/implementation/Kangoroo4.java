package hackerrank.implementation;

public class Kangoroo4 {
	static String Kangoro4 (int q1, int w1, int q2, int w2) {
		String  result ="";
		if(q1<w1 && q2<w2) {
			result = "NO";
		}else {
			if(w1 != w2 && (q1 -q2)% (w1-w2)==0) {
				result = "YES";
			}else {
				result = "NO";
			}
			
		}
		return result;
	}
	
	public static void main(String[] args) {
		int t1 = 0;
		int w1= 3;
		int t2 = 4;
		int w2 = 2;
		String h= Kangoro4(t1, w1, t2, w2);
		System.out.println(h);
				
	}
}
