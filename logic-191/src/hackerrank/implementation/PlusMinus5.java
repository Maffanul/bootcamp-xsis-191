package hackerrank.implementation;

public class PlusMinus5 {
	static void plusMinus5(int[] ard) {
		float nPositif = 0;
		float nNegatif =0;
		float w = 0;
		
		for (int q = 0; q < ard.length; q++) {
			if(ard[q]<0) {
				nNegatif++;
			}else if(ard[q]>0) {
				nPositif++;
			}else {
				w++;
			}
			System.out.println(nPositif/ard.length);
			System.out.println(nNegatif/ard.length);
			System.out.println(w/ard.length);
		}
	}
	public static void main(String[] args) {
		int[] ard= {2,-1,-9,4,0,3};
		
		plusMinus5(ard);
	}

}
