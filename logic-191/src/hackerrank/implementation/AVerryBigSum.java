package hackerrank.implementation;

public class AVerryBigSum {
	//method dengan mengembalikan nilai bertipe data long
		//variable ar bertype long array 1 dimensi
	static long averryBigSum(long[] ar) {//return value
		//variabel untuk menampung data
		long a=0; 
		
		for (int i=0; i<ar.length; i++) {
			//penambahan nilai b dengan index arr ke i
			a+=ar[i];
		}
		//pengembalian nilai ke a
		return a;
	}
	//method utama untuk mancetak
	public static void main(String[] args) {
		//variabel arr ber type data long dengan tambahan value
		long[] ar = {5,2,4,6};
		//variabel n untuk menampung nilai dari method averryBigSum(ar)
		long n = averryBigSum(ar);
		//print dari n
		System.out.println(n);
		
		
	}


}
