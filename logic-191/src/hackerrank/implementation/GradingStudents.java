package hackerrank.implementation;
//mencari penggenaan nilai yang mendekati 5 digenapkan menjadi 5 
//degan syarat <=3, dan nilai di atas 38
public class GradingStudents {
	static  int[] gradingStudents(int[] grade) {
		for (int i = 0; i < grade.length; i++) {
			if(grade[i]>=38) {
				if((5-grade[i]%5) < 3) {
					grade[i] =(grade[i]+(5-grade[i]%5));
				}
			}
		}
		return grade;
	}
	
	public static void main(String[] args) {
		int [] i = {73,67,38,33};
		int [] o = gradingStudents(i);
		System.out.println(o[0]);
		System.out.println(o[1]);
		System.out.println(o[2]);
		System.out.println(o[3]);
		System.out.println(73%5);
	}
}
