package hackerrank.implementation;

public class AVerryBigSum6 {
	static long bigBigsum (long[] ader) {
		long d=0;
		for (int p = 0; p < ader.length; p++) {
			d+=ader[p];
		}		
		
		return d;
	}
	
	public static void main(String[] args) {
		long [] ader = {3,3,5,6,7};
		long p = bigBigsum(ader);
		
		System.out.println(p);
	}
}
