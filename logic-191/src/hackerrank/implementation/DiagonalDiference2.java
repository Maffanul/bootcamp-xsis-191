package hackerrank.implementation;

public class DiagonalDiference2 {
	static int diagonalDiference2(int [][]ard) {
		int q1 = 0;
		int w1 = 0;
		for (int j = 0; j < ard.length; j++) {
			q1 +=ard [j][j];
			w1 +=ard [j][ard.length-1-j];
		}
		if(w1>q1) {
			return w1-q1;
		}else{
			return q1-w1;
		}
	}
	
	public static void main(String[] args) {
		int [][]ard= {{2,3,5},{5,2,3}};
		System.out.println(diagonalDiference2(ard));
	}
	
}
