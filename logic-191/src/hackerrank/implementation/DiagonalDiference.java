package hackerrank.implementation;

public class DiagonalDiference {
	static int diagonalDiference(int[][] arr) {//membuat method dengan pengembalian niali 
												// bertype int array [][] dua dimensi  bernama arr
		
		int a=0; // variabel untuk menampung niali a, variabel di luar perulangan karena akan di tambahkan terus 
		int b=0; // variabel untuk menampung niali dari b, variabel di luar  perulangan karena untuk di tambah terus
		
		for(int i=0; i<arr.length; i++) { // perulangan untuk mengakses arr 
			a+=arr[i][i];				// variabel a ke [i] dan [i], maka a akan di tambah
			b+=arr[i][arr.length-1-i]; // variabel b pada saat ke [i] dan [arr.length-1-i] dalam arti
										// panjang arr di kurang 1 dan dikurang i setiap perulangan yang berarti bertambah i nya
										// yang menyebabkan [i] yang ke dua terus menyamping ke kiri
			
		}
		if(a>b) {	// kondisi jika a lebih besar dari b maka , karena nilahi harus positif
			return a-b; // a akan di kurang b
		}else { // lainnya
			return b-a; // b di kurang dengan a
		}
	}
	public static void main(String[] args) {
		int [][] arr = {{13,12,15},{15,23,12}}; // untuk mengisi seacra manual 
												//int arr [][] {} array pertama atau a, {} selanjutnya untuk b
		System.out.println(diagonalDiference(arr)); // menampilkan dari method diagonaldiference();
	}
}
