package hackerrank.implementation;

public class PlusMinus3 {
	static void plusMinus3 (int[] are) {
		float nPositif = 0;
		float nNegatif = 0;
		float o = 0;
		
		for (int k = 0; k < are.length; k++) {
			if(are[k]<0) {
				nNegatif++;
			}else if(are[k]>0) {
				nPositif++;
			}else {
				o++;
			}
			System.out.println(nPositif/are.length);
			System.out.println(nNegatif/are.length);
			System.out.println(o/are.length);
		}
		
	}
	
	public static void main(String[] args) {
		int[] are= {2,-1,5,-5,0,3};
		
		plusMinus3(are);
	}

}
