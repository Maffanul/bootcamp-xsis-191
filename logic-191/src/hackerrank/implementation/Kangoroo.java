package hackerrank.implementation;
//adlaah 2 kanggoro yang melompat bersama
// dicari lompatan yang jatuhnya atau (x)tempatnya sama 
public class Kangoroo {
	static String kangoroo(int x1, int v1,int x2, int v2 ) {
		 
	        while(x1<=x2){
	            if(x1==x2){
	                return "YES";	                
	            } 
	            x1+=v1;
	            x2+=v2;
	        }        

	        return "NO";
	}
	
	public static void main(String[] args) {
		int t1 = 0;
		int l1 = 3;
		int t2 = 4;
		int l2 = 2;
		String h = kangoroo(t1, l1, t2, l2);
		System.out.println(h);
	}
}
