package hackerrank.implementation;

public class PlusMinus4 {
	static void plusMinus4(int[] ars) {
		float nPositif = 0;
		float nNegatif = 0;
		float p = 0;
		
		for (int a = 0; a < ars.length; a++) {
			if(ars[a]<0) {
				nNegatif++;
			}else if(ars[a]>0) {
				nPositif++;
			}else {
				p++;
			}
			System.out.println(nPositif/ars.length);
			System.out.println(nNegatif/ars.length);
			System.out.println(p/ars.length);
		}
	}
	public static void main(String[] args) {
		int[] ars = {-3,-8,0,1,4,5};
		
		plusMinus4(ars);
	}

}
