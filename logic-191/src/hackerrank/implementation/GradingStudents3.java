package hackerrank.implementation;

public class GradingStudents3 {
	static int[] gradingStudent3 (int[] grades) {
		for (int k = 0; k < grades.length; k++) {
			if(grades[k] >=38) {
				if(grades[k]+(5-grades[k]%5)-grades[k]<3) {
					grades[k]=(grades[k]+(5-grades[k]%5));
				}
			}
			
		}
		return grades;
	}
	
	public static void main(String[] args) {
		int [] k = {73, 64,33,78};
		int [] o = gradingStudent3(k);
		System.out.println(o[0]);
		System.out.println(o[1]);
		System.out.println(o[2]);
		System.out.println(o[3]);
	}

}
