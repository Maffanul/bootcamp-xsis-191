package hackerrank.implementation;

public class Kangoroo2 {
	static String kangoro2 (int y1, int z1, int y2, int z2) {
		String result = "";
		if(y1<z1 && y2<z2) {
			result = "NO";
		}else {
			if(z1 != z2 &&(y2 -y1)%(z1-z2)==0) {
				result = "YES";
				
			} else {
				result = "NO";
			}
		}
		return result;
	}
	
	public static void main(String[] args) {
		int r1 = 0;
		int t1=3;
		int r2=4;
		int t2=2;
		
		String h = kangoro2(r1, t1, r2, t2);
		System.out.println(h);
	}
	
}
