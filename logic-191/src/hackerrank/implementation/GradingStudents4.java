package hackerrank.implementation;

public class GradingStudents4 {
	static int [] gradingStudent4 (int[] grades) {
		for (int a = 0; a < grades.length; a++) {
			if(grades[a] <=38) {
				if(grades[a]+(5- grades[a]%5)-grades[a]<3) {
					grades[a] =(grades[a]+5-grades[a]%5);
				}
			}
		}
		return grades;
	}
	
	public static void main(String[] args) {
		int [] a = {76,38,56,78};
		int [] h = gradingStudent4(a);
		
		System.out.println(h[0]);
		System.out.println(h[1]);
		System.out.println(h[2]);
		System.out.println(h[3]);
	}

}
