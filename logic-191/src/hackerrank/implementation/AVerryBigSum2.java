package hackerrank.implementation;


public class AVerryBigSum2 {
	public static long aVerryBigSum2(long [] n) {
		long a = 0;
		for(int i=0; i<n.length; i++) {
			a = a+n[i];
		}
		return a;
	}
	public static void main(String[] args) {
		long [] n = {1,3,4,6,8};
		System.out.println(aVerryBigSum2(n));
	}
	

}
