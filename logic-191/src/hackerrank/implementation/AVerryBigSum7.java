package hackerrank.implementation;

public class AVerryBigSum7 {
	static long bigSum (long [] arr) {
		long a=0;
		
		for (int i = 0; i < arr.length; i++) {
			a+= arr[i];
		}
		return a;
	}
	public static void main(String[] args) {
		long [] arr = {12,1,2,3};
		long s = bigSum(arr);
		System.out.println(s);
		
		
	}
}
