package hackerrank.implementation;

public class AVerryBigSum4 {
	static long theBigSum(long[] ade) {
		long  c = 0;
		for (int l = 0; l < ade.length; l++) {
			c+=ade[l];
		}
		
		return c;
	}
	
	public static void main(String[] args) {
		long [] ade = {4,67,3,1};
		long n = theBigSum(ade);
		System.out.println(n);
	}
}
