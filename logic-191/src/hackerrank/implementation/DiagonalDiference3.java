package hackerrank.implementation;

public class DiagonalDiference3 {
	static int diagonalDiference3(int[][] ars) {
		int s1 = 0;
		int d1 = 0;
		for (int k = 0; k < ars.length; k++) {
			s1 += ars[k][k];
			d1 += ars[k][ars.length-1-k];
		}
		if(d1>s1) {
			return d1-s1;
		}else {
			return s1-d1;
		}
	}

	
	public static void main(String[] args) {
		int[][] ars = {{12,56,32},{13,24,89}};
		System.out.println(diagonalDiference3(ars));
	}
}
