package hackerrank.implementation;

public class DiagonalDiference5 {
	static int diagonalDiference5(int[][] aw) {
		int a = 0;
		int b = 0;
		
		for (int q = 0; q < aw.length; q++) {
			a += aw[q][q];
			b += aw[q][aw.length-1-q]; 
		}
		if(a>b) {
			return a-b;
		}else {
			return b-a;
		}
	}
	
	public static void main(String[] args) {
		int[][] aw = {{12,14,2,15},{1,23,1,23}};
		System.out.println(diagonalDiference5(aw));
	}
}
