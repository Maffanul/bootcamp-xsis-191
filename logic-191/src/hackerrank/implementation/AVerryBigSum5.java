package hackerrank.implementation;

public class AVerryBigSum5 {
	static long bigSums(long[] arre) {
		long c =0;
		for (int m = 0; m < arre.length; m++) {
			c += arre[m];
			
		}		
		return c;
	}
	
	public static void main(String[] args) {
		long [] arre = { 4,7,5,3};
		long o = bigSums(arre);
		System.out.println(o);
	}
}
