package hackerrank.implementation;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CompareTheTriplets4 {
	static List<Integer> compareTheTriplest (List<Integer> a, List<Integer>b){
		List<Integer> result = new ArrayList<>();
		
		result.add(0);
		result.add(0);
		int nA=0;
		int nB=0;
		
		for (int i = 0; i < a.size(); i++) {
			if(a.get(i)>b.get(i)) {
				nA++;
				result.set(0, nA);
			}
			if(a.get(i)<b.get(i)) {
				nB++;
				result.set(1, nB);
			}
		}
		return result;
	}
	
	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		List<Integer> list = new ArrayList<>();
		while(scn.hasNextInt())
			list.add(scn.nextInt());
	}
}
