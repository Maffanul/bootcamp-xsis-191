package hackerrank.implementation;

public class DiagonalDiference4 {
	static int diagonalDiference4(int[][] asd) {
		int z1 = 0;
		int x1 = 0;
		
		for (int l = 0; l < asd.length; l++) {
			z1 += asd[l][l];
			x1 += asd[l][asd.length-1-l];
		}
		if(x1>z1) {
			return x1-z1;
		}else {
			return z1-x1;
		}
	}

	public static void main(String[] args) {
		int[][] asd = {{9,14,15,16},{13,15,12,14}};
		System.out.println(diagonalDiference4(asd));
	}
}
