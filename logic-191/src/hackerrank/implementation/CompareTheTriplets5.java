package hackerrank.implementation;

import java.util.ArrayList;
import java.util.List;

public class CompareTheTriplets5 {
	static List<Integer> compareTheTriple5 (List<Integer> g , List<Integer> h){
		List<Integer> result = new ArrayList<Integer>();
		
		result.add(0);
		result.add(0);
		
		int nG = 0;
		int nH =0;
		
		for (int s = 0; s < g.size(); s++) {
			if(g.get(s)> h.get(s)) {
				nG++;
				result.set(0, nG);
				
			if(g.get(s)<h.get(s)) {
				nH++;
				result.set(nH, 0);
			}
			}
		}
		return result;
	}
	
	public static void main(String[] args) {
		List<Integer> g = new ArrayList<Integer>();
		g.add(11);
		g.add(14);
		g.add(12);
		
		List<Integer> h = new ArrayList<Integer>();
		h.add(12);
		h.add(13);
		h.add(15);
		
		for(Integer hasil : compareTheTriple5(g, h)) {
			System.out.println(hasil +"\t");
		}
			
	}
}
