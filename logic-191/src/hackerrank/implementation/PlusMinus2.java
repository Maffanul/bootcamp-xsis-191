package hackerrank.implementation;

public class PlusMinus2 {
	static void plusMinus2(int[] ar) {
		float nPossitif =0;
		float nNegatif = 0;
		float m = 0;
		
		for (int j = 0; j < ar.length; j++) {
			if(ar[j]<0) {
				nNegatif++;
				
			}else if(ar[j]>0) {
				nPossitif++;
				
			}else {
				m++;
			}
			System.out.println(nPossitif/ar.length);
			System.out.println(nNegatif/ar.length);
			System.out.println(m/ar.length);
		}
	}
	
	public static void main(String[] args) {
			int [] ar = {1,-1,0,-9,3,6};
			
			plusMinus2(ar);
	}
}
