package hackerrank.implementation;

public class PlusMinus {
	
	static void plusMinus(int[] arr) {
		float nPositve =0;
		float nNegative = 0;
		float n = 0;
		
		for (int i = 0; i < arr.length; i++) {
			if(arr[i]<0) {
				nNegative++;
			}else if(arr[i]>0) {
				nPositve++;
			}else {
				n++;
			}
			System.out.println(nPositve/arr.length);
			System.out.println(nNegative/arr.length);
			System.out.println(n/arr.length);
		}
	}
	
	public static void main(String[] args) {
		int [] arr = {1,-2,3,0,3,-9};
		
		plusMinus(arr);
	}

}
