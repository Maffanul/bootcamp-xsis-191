package hackerrank.implementation;

public class AVerryBigSum3 {
	//method dengan mengembalikan nilai bertipe data long
		//variable are bertype long array 1 dimensi
	static long  perTambahan (long[] are) {
		//variabel untuk menampung data
		long c =0;
		//perulangan untuk menambah nilai c dari panjang array are ke k
		for (int k = 0; k < are.length; k++) {
			//penambahan nilai ke variabel c
			c += are[k];
		}
		//pengembalian ilai ke c
		return c;
	}
	//method untuk mencetak
	public static void main(String[] args) {
		//variabel are dengan pengisian nilai 
		long [] are = {4,3,2,1};
		//variabel untuk menampung nilai dari pertambahan(are)
		long m = perTambahan(are);
		//mencetak nilai dari variabel m
		System.out.println(m);
	}
}
