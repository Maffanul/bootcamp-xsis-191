package hackerrank.implementation;

import java.util.ArrayList;
import java.util.List;
//menghitung banyak coklat bar yang dapat dibagi
// ada 3 variabel, array coklat atau nilai di bar coklat
// terus m untuk total dari d bar coklat
public class BirthdayCocolate {
	 static int birthday(List<Integer> s, int d, int m) {
	        int count=0;
	        for(int i=0; i<=s.size()-m; i++){
	            int total = 0;
	            for(int j=0; j<m; j++){
	               total = s.get(i)+s.get(j);	
	            }
	            if(total==d){
	                count++;
	            }
	        }
	        return count;
	   

}
	 public static void main(String[] args) {
		List<Integer> s = new ArrayList<Integer>();
		s.add(1);
		s.add(2);
		s.add(1);
		s.add(3);
		s.add(2);
		int d=3;
		int m=2;
		System.out.println(birthday(s, d, m));
	}
}