package hackerrank.implementation;

import java.util.ArrayList;
import java.util.List;

public class CompareTheTriplets3 {
	static List<Integer> compare (List<Integer>a , List<Integer>b){
		List<Integer> result = new ArrayList<Integer>();
		
		result.add(0);
		result.add(0);
		
		int nA=0;
		int nB=0;
		
		for (int i = 0; i <a.size(); i++) {
			if(a.get(i)>b.get(i)) {
				nA++;
				result.set(0, nA);
			}
			if(a.get(i)<b.get(i)) {
				nB++;
				result.set(1, nB);
			}
		}
		return result;
	}
	
	public static void main(String[] args) {
		List<Integer> a =new ArrayList();
		a.add(12);
		a.add(14);
		a.add(15);
		
		List<Integer> b=new ArrayList();
		b.add(11);
		b.add(15);
		b.add(11);
		
		for(Integer hasil : compare(a, b)) {
			System.out.println(hasil);
		}
	}
}
