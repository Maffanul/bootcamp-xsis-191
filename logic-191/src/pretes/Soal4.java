package pretes;

import java.util.List;
import java.util.Scanner;

public class Soal4 {
	//static Scanner scn;
	static void soal04(int [] n, int m) {
		int [] mam = new int [n.length-m];
		for (int i = 0; i < n.length-m; i++) {
			int jumlah = 0;
			for (int j = i; j <m+i; j++) {
				jumlah =jumlah+n[j];
			}
			mam[i]=jumlah;
		}
		int min = mam[0];
		int max = min;
		for (int i = 0; i < mam.length; i++) {
			if(mam[i]<min) {
				min=mam[i];
			}
			if(mam[i]>max) {
				max=mam[i];
			}
		}
		System.out.println("min: "+min+ "| max: "+max);
	}
	public static void main(String[] args) {		
		int [] n= new int [] {10,20,30,40,50,60,70};
		int m =4;
		//int b =0;
		
		soal04(n, m);
	}

}
