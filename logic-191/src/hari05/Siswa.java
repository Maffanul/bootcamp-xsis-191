package hari05;

public class Siswa {
	int id;
	String nama;
	String alamat;
	String kelas;
	
	public Siswa(int id, String nama, String alamat, String kelas) {
		this.id=id;
		this.nama=nama;
		this.alamat=alamat;
		this.kelas=kelas;
	}
	public void showSis() {
		System.out.println("id \t"+id);
		System.out.println("nama \t"+nama);
		System.out.println("alamat \t"+alamat);
		System.out.println("kelas \t"+kelas);
		
	}
}
