package hari05;

public class Kendaraan {
	String id;
	String jenis;
	String namaKendaraan;
	String pemilik;
	
	public Kendaraan(String id, String jenis, String namaKendaraan, String pemilik) {
		this.id=id;
		this.jenis=jenis;
		this.namaKendaraan=namaKendaraan;
		this.pemilik=pemilik;
	}
	public void ShowKedaraan() {
		System.out.println("Id\t"+id);
		System.out.println("Jenis\t"+jenis);
		System.out.println("Nama Kendaraan\t"+namaKendaraan);
		System.out.println("Pemilik\t"+pemilik);
	}
}
