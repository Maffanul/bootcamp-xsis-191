package hari05;

public class OrangMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Orang org1 = new Orang(1, "affan", "ciledug", "lakilaki", 23);
		System.out.println("Data orang pertama: ");
		org1.showData();
		
		Orang org2 = new Orang(2, "adit", "ciliwung", "laik",22);
		System.out.println("Data orang ke dua: ");
		org2.showData();
		
		Orang org3 = new Orang(3,"yuni", "pinang");
		System.out.println("Data orang ke 3 : ");
		org3.showData();
		
		
		Orang org7 = new Orang();
		
		
		Orang org4=org1;
		System.out.println("Data orang ke 4: ");
		org4.showData();
		
		Sekolah sis1=new Sekolah(1, "SMP1","cikareng");
		System.out.println("Data sekolah 1: ");
		sis1.showData2();
		
		Siswa ani = new Siswa(1,"anisa","jawa","ipa");
		System.out.println("Data siswa ani: ");
		ani.showSis();
		
		Kendaraan ani2 = new Kendaraan("B41K","motor", "honda", "bowo");
		System.out.println("data kendaraan: ");
		ani2.ShowKedaraan();
				
		
	}

}
