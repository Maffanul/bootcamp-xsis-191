package hari05;

public class Orang {
	int id;
	String nama;
	String alamat;
	String jk;
	int umur;
	
	//constructor
	public Orang() {
		
	}
	//constructor adalah method yang di panggil saat instansiasi
	public Orang(int id, String nama, String alamat,String jk, int umur) {
		this.id=id;
		this.nama=nama;
		this.alamat=alamat;
		this.jk=jk;
		this.umur=umur;
	}
	//constructor
	public Orang(int id, String nama, String alamat) {
		this.id=id;
		this.nama=nama;
		this.alamat=alamat;
	}
	//polomorfisme adalaha banyak construcror dengan penggunaan yang berbeda 
	public void showData() {
		System.out.println("ID \t:"+this.id);
		System.out.println("Nama \t: "+this.nama);
		System.out.println("Alamat \t: "+this.alamat);
		System.out.println("JK \t: "+this.jk);
		System.out.println("Umur \t"+this.umur);
		
	}
	
}
