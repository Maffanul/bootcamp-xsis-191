package hari05;
import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class CompareTheTriplets {

	 public static void main(String[] args) throws IOException {
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

	        String[] aItems = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

	        List<Integer> a = new ArrayList<>();

	        for (int i = 0; i < 3; i++) {
	            int aItem = Integer.parseInt(aItems[i]);
	            a.add(aItem);
	        }

	        String[] bItems = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

	        List<Integer> b = new ArrayList<>();

	        for (int i = 0; i < 3; i++) {
	            int bItem = Integer.parseInt(bItems[i]);
	            b.add(bItem);
	        }

	        List<Integer> result = compareTriplets(a, b);

	        for (int i = 0; i < result.size(); i++) {
	            bufferedWriter.write(String.valueOf(result.get(i)));

	            if (i != result.size() - 1) {
	                bufferedWriter.write(" ");
	            }
	        }

	        bufferedWriter.newLine();

	        bufferedReader.close();
	        bufferedWriter.close();
	    }
	

	 static List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {

	        List <Integer> result = new ArrayList();

	        result.add(0);
	        result.add(0);

	        int nAlice = 0;
	        int nBob = 0;

	        for(int i=0; i<a.size(); i++){
	            if(a.get(i)>b.get(i)){
	                nAlice++;
	                result.set(0,nAlice);
	            }
	            if(a.get(i)<b.get(i)){
	                nBob++;
	                result.set(1,nBob);
	            }
	        }
	        return result;

	    }

}
