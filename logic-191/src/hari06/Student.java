package hari06;

public class Student extends Person{

	public String major;
	public String grade;
	//constructor= method yang di panggil saat instansiasi
	public void sis() {
		System.out.println("dari student adalah jurusan dan nilai");
	}
	public Student() {
		
	}
	
	public Student (int id, String nama, String adress, String gender, String major, String grade) {
		this.id=id;
		this.nama=nama;
		this.adress=adress;
		this.gender=gender;
		this.major=major;
		this.grade=grade;
	}
	
	public void showStudent() {
		System.out.println("ID:\t"+id);
		System.out.println("Nama:\t"+nama);
		System.out.println("Alamat:\t"+adress);
		System.out.println("Jenis kelamin:\t"+gender);
		System.out.println("jurusan:\t"+major);
		System.out.println("Nilai:\t"+grade);
	}

	
}
