package hari06;

public class OverLoading {
	//over loading datatype
	static int jm1(int a, int b) {
		return a+b;
	}
	static double jml1(double a, double b) {
		return a-b;
	}
	
	// overloading changing of arguments
	static int jml2(int a, int b) {
		return a+b;
		
	}
	
	static int jml2(int a, int b, int c) {
		return a+b+c;
	}
	public static void main(String[] args) {
		//overloading data type
		System.out.println(OverLoading.jm1(2, 3));
		
		System.out.println(OverLoading.jml1(2, 3));
		
		//overloading changing of arguments
		System.out.println(OverLoading.jml2(1, 2));
		
		System.out.println(OverLoading.jml2(1, 2, 3));
	}

}
