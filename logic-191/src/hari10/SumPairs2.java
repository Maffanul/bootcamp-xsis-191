package hari10;

public class SumPairs2 {
	static int sumPairs2(int n, int k, int[] ar) {
		int count = 0;
		for (int i = 0; i < ar.length; i++) {
			for (int j = i+1; j < ar.length; j++) {
				if((ar[i]+ar[j]) %k==0) {
					count++;
				}
			}
		}
		return count;
	}
	

	public static void main(String[] args) {
	int n =6;
	int k =3;
	int[] ar= {1,3,2,6,2,1};
	
	System.out.println(sumPairs2(n, k, ar));
	}

}
