package hari10;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Migra1 {

	static int migra(List<Integer> arr) {
		Map<Integer, Integer> map = new HashMap<>();
		for (int i = 0; i < arr.size(); i++) {		
			if(map.containsKey(arr.get(i))) {
				int n = map.get(arr.get(i));
				n++;
				map.put(arr.get(i), n);
			}else {
				map.put(arr.get(i), 1);
			}
		}
		
		int max = 0;
		int key = 0;
		for (Map.Entry<Integer, Integer> item	 : map.entrySet()) {
			if(item.getValue()>max) {
			max = item.getValue();
			key = item.getKey();
			}
			if(item.getValue()==max && item.getKey()<key) {
				key = item.getKey();
			}
		}
		return key;
	}
	
	
	
	
	public static void main(String[] args) {
		List<Integer> list = new ArrayList<Integer>(); 
			list.add(1);
			list.add(2);
			list.add(3);
			list.add(4);
			list.add(5);
			list.add(4);
			list.add(3);
			list.add(2);
			list.add(1);
			list.add(3);
			list.add(4);
			
		System.out.println(migra(list));
	}
}
