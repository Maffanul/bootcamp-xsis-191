package hari07;

public class ShapeClass01 {

	public static void main(String[] args) {
		Shape01 sap01 = new Ratangle01();
		Shape01 sap02 = new Box01();
		Shape01 sap03 = new Oval01();
		Shape01 sap04 = new Ratangle02();
		Shape01 sap05 = new Circle02();
		
		sap01.draw();
		sap02.draw();
		sap03.draw();
		sap04.draw();
		sap05.draw();
		
		System.out.println();
		
		sap01.size1();
		System.out.println("size from ratangle : "+sap01.size1());
		sap02.size1();
		System.out.println("size from circle : "+sap02.size1());
		sap03.size1();
		System.out.println("size from oval : "+sap03.size1());
		sap04.size1();
		System.out.println("sisze from ratangle02: "+sap04.size1());
		sap05.size1();
		System.out.println("size from corcle02: "+sap05.size1());
	}
}
