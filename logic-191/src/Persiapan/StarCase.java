package Persiapan;

public class StarCase {

	static void bintangKanan(int n) {
		
		for (int i = 0; i < n; i++) {
			for (int j = 2*(n-i); j >=0; j--) {
				System.out.print(" ");
			}
			for (int j = 0; j <= i; j++) {
				System.out.print("* ");
			}
			System.out.println("\n");
		}
	}
	static void bintangKiri(int n) {
		
		for (int i = 0; i < n; i++) {
			
			for (int j = 0; j <= i; j++) {
				System.out.print("* ");
			}
			System.out.println("\n");
		}
	}
	static void bintangTengah(int n) {
		
		for (int i = 0; i < n; i++) {
			for (int j = n-i; j >=0; j--) {
				System.out.print(" ");
			}
			for (int j = 0; j <= i; j++) {
				System.out.print("* ");
			}
			System.out.println("\n");
		}
	}
	static void bintangKebalik(int n) {
		for (int i = 0; i <=n; i++) {
			for (int j = n-i; j >=0; j--) {
				System.out.print("* ");
			}
			System.out.println("\n");
		}
	}
	public static void main(String[] args) {
		int n=5;
		bintangKanan(n);
		bintangKiri(n);
		bintangTengah(n);
		bintangKebalik(n);
	}

}
