package Persiapan;

public class PageBook {
	// menghitung page sekarang ke page yang di inginkan(munfdur ke belakang)
	static int pageBook(int n, int k) {
		if(n%2==0) {//jika genap maka ditambah 1, 
					//karena dalam satu halaman page kanan ganjil dan kiri genap
			n++;
		}
		if(k%2==0) {
			return Math.min((k-0)/2	, (n-1-k)/2);
		}
		
		return Math.min((k-1)/2, (n-k)/2);
	}
	
	public static void main(String[] args) {
		int n=5; int k=3;
		System.out.println(pageBook(n, k));
	}

}
