package Persiapan;

public class Soal9 {

	static int sudutJam(double jam, double menit) {
		if(jam<0||menit<0||jam>12||menit>60) {
			System.out.println("inputan salah");
		}
		if(jam==12) {
			jam=0;
		}
		if(menit==60) {
			menit=0;
		}
		
		//menghitung dari jarum jam dan menit
		//dengan refrence 12
		int sudut_jam=(int)(0.5 *(jam*60+menit));
		int sudut_menit=(int)(6*menit);
		
		//perbedaan sudut antara jam dan menit
		int sudut = Math.abs(sudut_jam -sudut_menit);
		
		//sudut terkecil dari dua bentukan
		sudut=Math.min(360-sudut, sudut);
		
		return sudut;
	}
	public static void main(String[] args) {
		System.out.println(sudutJam(3, 40)+" Derajat");
	}
}
