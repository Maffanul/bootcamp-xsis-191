package Persiapan;

import java.util.Arrays;
//untuk mutar array yang depan ke belakang
public class Soal3 {
	static void methodSoal03(int n, int[] deret) {
	//	System.out.println(Arrays.toString(deret));
		int temp;
		for (int i = 0; i <n; i++) {
			temp = deret[0];
			for (int j = 0; j < deret.length-1; j++) {
				deret[j]=deret[j]+1;
				
			}
			deret[deret.length-1]=temp;
		}
		System.out.println(Arrays.toString(deret));
	}
				
	public static void main(String[] args) {
		methodSoal03(2, new int[] {3,4,5,6,7,7,8,1});
	}

}
