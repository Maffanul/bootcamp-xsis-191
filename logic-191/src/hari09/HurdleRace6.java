package hari09;

public class HurdleRace6 {
	public static int hurdleRace6 (int k, int[] heigth) {
		// membuat variabel untuk menampung nilai maxq
		int maxq = heigth[0];
		// perulangan untuk menemukan nilai dari maxq
		for (int i = 0; i < heigth.length; i++) {
			if(heigth[i]>maxq)
				maxq = heigth[i];
		}
		// jika telah di temukan maxq maka akan dilakukan proses untuk ke k
		if(k<maxq)
			return maxq -k;
		else 
			return 0;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(hurdleRace6(9, new int[] {2,2,8,7,6}));
	}

}
