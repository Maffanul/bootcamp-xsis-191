package hari09;

public class Factorial3 {
	
	public static int factorial3(int n) {
		if(n==0)// kondisi jika terpenuhi maka aka di eksekusi return 1;
			return 1;
		
		return n*factorial3(n-1); // jika kondisi belum ter penuhi maka n di kurang 1 dan disimpan ke threaddan kembali ke kondisi sampai terpenuhi
									// jika telah terpenuhi maka n yang telah tersimpan pada thread akan di kalikan dengan n
	}
	
	public static void main(String[] args) {
		System.out.println(factorial3(7));
	}

}
