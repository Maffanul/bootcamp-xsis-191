package hari09;

public class HurdleRace4 {
	public static int hurdleRace4(int k, int [] heigth) {
		// variabel untuk menampung nilai maxo
		int maxo = heigth[0]; // nilai maxo awal adalah 0
		
		// perulangan untuk menemukan nilai maxo
		for (int a = 1; a < heigth.length; a++) {
			if(heigth[a]>maxo) {
				maxo =heigth[a];
			}
		}
		// jika maxo telah di dapatkan makan akan di jumlahkan
		if(k<maxo) {
			return maxo -k;
		}else {
			return 0;
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(hurdleRace4(8, new int [] {5,4,7,7,2}));
	}

}
