package hari09;

import java.util.Scanner;

public class StringComparition {
	public static void main(String[] args) {
		
	
	 Scanner sc = new Scanner(System.in);
     String s = sc.next();
     int k = sc.nextInt();     
     String smallest = "";
     String largest = "";

     for (int i = 0; i <= s.length() - k; i++) {
         String substr = s.substring(i,  i + k);
         if (substr.compareTo(smallest) < 0 || smallest.isEmpty()) {
             smallest = substr;
         }
         if (substr.compareTo(largest) > 0 || largest.isEmpty()) {
             largest = substr;
         }
     }

     System.out.println(smallest+"\n"+largest);
     
 }
}
