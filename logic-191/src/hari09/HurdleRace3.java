package hari09;

public class HurdleRace3 {
	
	public static int hurdleRace3(int k, int[] heigth) {
		// variabel untuk menampung nilai max
		int maxi = heigth[0];
		// perulangan untuk mencari nilai max
		for (int j = 1; j < heigth.length; j++) {//1 karena nilai maxi adalah 0
			if(heigth[j]>maxi) {
				maxi =heigth[j];
			}
		}
		// selanjutnya mencari nialilompatan hurdle
		if(k<maxi) {
			return maxi -k;
		}else {
			return 0;
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(hurdleRace3(6, new int[] {2,5,7,4,2}));
	}

}
