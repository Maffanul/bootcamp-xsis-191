package hari02;

import hari03.Array;

public class Nomor5_1 {

	public static void pecahKata(String word) {
		String [] array = word.split("(?=[A-Z])"); // di pisah berdasarkan sepasi
		//String [] array = word.split(" "); berdasarkan sepasi 
		for (int i = 0; i < array.length; i++) {
			String [] item = array[i].split("");// setelah di pisah berdasarkan sepasi maka akan di pisah lagi berdasarkan huruf
			for (int j = 0; j < item.length; j++) {
				if(j>0 && j<item.length-1) { // kata tengan antara paling belakang dan paling depan berisi bintang
					System.out.print("*");
				}else {
					System.out.print(item[j]);
				}
			}
			System.out.print(" ");
		}
	}
	public static void main(String[] args) {
		String word = "AkuSukaKamu";
		pecahKata(word);
		

	}

}
