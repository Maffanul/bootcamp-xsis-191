package hari02;

import java.util.Scanner;

public class Nomor5 {
	static Scanner scn;
	public static void main(String[] args) {
		scn = new Scanner(System.in); // scan text
		System.out.println("masukan karta: ");
		String text = scn.nextLine(); //mengambil text dari masukan
		
		String [] array = text.split(" "); //memecah dari inputan text dengan split" " berdasarkan sepasi kedalam variabel array
		
		for(int i=0; i<array.length; i++) {  //menampilkan array dengan for, i adalah index. i lebih kecil dari panjang kata(array yang sudah di split)
			String[] item = array[i].split(""); //memecah dari array berdasarkan item dengan ""
			for(int j=0; j<item.length; j++) { //menampilkan item dengan increment dari panjang item
				if(j>0 && j<item.length-1) {//jika j lebih dari 0 dan j kurang dari lenght kurangi 1
					System.out.print("*");
				}else {
					System.out.print(item[j]);
				}
			}
			System.out.print(" ");
		}
		
	}

}
