package hari01;

import java.util.Scanner;

public class SoalLooping {
	static Scanner scn;

	public static void main (String[] args) {
		// nomor 1
		
		soal1();
		System.out.println();
		soal2();
		System.out.println();
		soal3();
		System.out.println();
		soal4();
		System.out.println();
		soal5();
		System.out.println();
		soal6();
		System.out.println();
		soal7();
		System.out.println();
		soal8();
		System.out.println();
		soal9();
		System.out.println();
		soal10();
	}
	public static void soal1() {
		
		int a = 1;
		System.out.print("soal 1 : ");
		for(int i = 0; i <7; i++) {
			System.out.print(a +" ");
			a+=2;
		}
	}
	public static void soal2() {
		
		int b = 2;
		System.out.print("soal 2 : ");
		for(int i = 0; i<7; i++) {
			System.out.print(b +" ");
			b+=2;
		}
	}
	public static void soal3() {
		
		int c = 1;
		System.out.print("soal 3 : ");
		for(int i = 0; i <7; i++) {
			System.out.print(c + " ");
			c+=3;
		}
	}
	public static void soal4() {
		int d = 7;
		System.out.print("soal 4 : ");
		for(int i = 0; i < 7; i++) {
			System.out.print(d + " ");
			d+=6;
		}
	}
	public static void soal5() {
		
		int e = 1;
		System.out.print("soal 5 : ");
		
		for(int i=0; i<7; i++) {
			if(i==2||i==5) {
				System.out.print("* ");
			}else {
				System.out.print(e+" ");
			e+=4;
			}
			
		}
	}
	public static void soal6() {
		int f = 1;
		System.out.print("soal 6 : ");
		
		for (int i = 0; i<7; i++) {
			if(i == 2 || i == 5) {
				System.out.print("* ");
			}else {
				System.out.print(f+" ");
				
			}f+=4;
		}
	}
	public static void soal7() {
		int g = 2;
		System.out.print("soal 7 : ");
		for (int i = 0; i<7; i++) {
			System.out.print(g +" ");
			g+=g;
		}
	}
	public static void soal8() {
		int h = 3;
		System.out.print("soal 8: ");
		for (int i=0; i<7; i++) {
			System.out.print(h+" ");
			h*=3;
		}
	}
	public static void soal9() {
		int j = 4;
		System.out.print("soal 9 : ");
		for(int i = 0; i<7; i++) {
			if(i==2||i==5) {
				System.out.print("* ");
			}else {
				System.out.print(j+" ");
				j*=4;
			}
		}
	}
	public static void soal10() {
		int k = 3;
		System.out.print("soal 10: ");
		for(int i=0; i<7; i++) {
			if(i==3) {
				System.out.print("XXX ");
			}else {
				System.out.print(k+" ");
				
			}k*=3;
		}
	}
}
