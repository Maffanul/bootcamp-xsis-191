1.
SELECT "NM_FAKULTAS", "NM_JURUSAN"
FROM public."Fakultas", "Jurusan";
2.	
SELECT t1."NM_FAKULTAS", t2."NM_JURUSAN"
FROM public."Fakultas" t1 INNER JOIN public."Jurusan" t2 ON t1."KD_FAKULTAS" =T2."KD_FAKULTAS"
WHERE "NM_FAKULTAS" like '%if%';
3.		
SELECT t1."NM_FAKULTAS", t2."NM_JURUSAN"
FROM public."Fakultas" t1 INNER JOIN public."Jurusan" t2 ON t1."KD_FAKULTAS" =T2."KD_FAKULTAS"
WHERE "NM_JURUSAN" like '%ti%';
4.		
select t1."NM_FAKULTAS" , count(t2."KD_JURUSAN") as "JML_JURUSAN"
from public."Fakultas" t1 
left join public."Jurusan" t2 on t1."KD_FAKULTAS" = t2."KD_FAKULTAS"
group by t1."NM_FAKULTAS";
5.
select t1."NM_FAKULTAS" , count(t2."KD_JURUSAN") as "JML_JURUSAN"
from public."Fakultas" t1 
left join public."Jurusan" t2 on t1."KD_FAKULTAS" = t2."KD_FAKULTAS"
group by t1."NM_FAKULTAS"
having count(t2."KD_FAKULTAS")>1;
6.				
select t1."NM_FAKULTAS" , count(t2."KD_JURUSAN") as "JML_JURUSAN"
from public."Fakultas" t1 
left join public."Jurusan" t2 on t1."KD_FAKULTAS" = t2."KD_FAKULTAS"
group by t1."NM_FAKULTAS"
having count(t2."KD_FAKULTAS")=0;
7.
select t1."NM_MK", t2."NM_JURUSAN", t3."NM_FAKULTAS"
from public."Matakuliah" t1 
inner join public."Jurusan" t2 on t1."KD_JURUSAN" = t2."KD_JURUSAN"
inner join public."Fakultas" t3 on t2."KD_FAKULTAS" = t3."KD_FAKULTAS";
8.//
select t1."NM_JURUSAN", count(t2."KD_MK") as "JML_MK"
from "Jurusan" t1
full join "Matakuliah" t2 on t1."KD_JURUSAN" = t2."KD_JURUSAN"
group by t1."NM_JURUSAN";
9.
select t1."NM_JURUSAN", count(t2."SKS") as "JML_MK"
from "Jurusan" t1
left join "Matakuliah" t2 on t1."KD_JURUSAN" = t2."KD_JURUSAN"
group by t1."NM_JURUSAN"
having count(t2."SKS")=2;
10.
select t1."NM_JURUSAN", count(t2."SKS") as "JML_MK"
from "Jurusan" t1
left join "Matakuliah" t2 on t1."KD_JURUSAN" = t2."KD_JURUSAN"
group by t1."NM_JURUSAN"
having count(t2."SKS")=3;
11.
select t3."NM_FAKULTAS",t2."NM_JURUSAN" 
from "Matakuliah" t1
full join "Jurusan" t2 on t1."KD_JURUSAN" = t2."KD_JURUSAN"
left join "Fakultas" t3 on t2."KD_FAKULTAS" = t3."KD_FAKULTAS"
where t1."NM_MK" is null;
12.
select t1."NM_MK", t2."NM_JURUSAN", t3."NM_FAKULTAS"
from "Matakuliah" t1
inner join "Jurusan" t2 on t1."KD_JURUSAN" =t2."KD_JURUSAN"
inner join "Fakultas" t3 on t2."KD_FAKULTAS" = t3."KD_FAKULTAS"
where t1."NM_MK" like '%is%';
13.
select "ALAMAT" as "NM_KOTA" , count("KD_DOSEN")
from "Dosen"
group by "NM_KOTA";
14.
select t1."NM_DOSEN", t2."NM_MK", t3."NM_JURUSAN", t4."NM_FAKULTAS", t5."NM_KELAS",t6."NM_RUANG"
from "Fakultas" t4
join "Jurusan" t3 on t4."KD_FAKULTAS" =t3."KD_FAKULTAS"
join "Matakuliah" t2 on t3."KD_JURUSAN" = t2."KD_JURUSAN"
join "Kelas" t5 on t2."KD_MK" = t5."KD_MK"
join "Ruang" t6 on t5."KD_RUANG" = t6."KD_RUANG"
join "Dosen" t1 on t5."KD_DOSEN"= t1."KD_DOSEN";
15.
select t1."NM_DOSEN", t2."NM_MK", t3."NM_JURUSAN", t4."NM_FAKULTAS"
from "Fakultas" t4
join "Jurusan" t3 on t4."KD_FAKULTAS" =t3."KD_FAKULTAS"
join "Matakuliah" t2 on t3."KD_JURUSAN" = t2."KD_JURUSAN"
join "Kelas" t5 on t2."KD_MK" = t5."KD_MK"
join "Dosen" t1 on t5."KD_DOSEN"= t1."KD_DOSEN";
15.
select t1."NM_DOSEN", count (t2."NM_MK") as "JML_MK"
from "Dosen" t1
join "Kelas" t3 on t1."KD_DOSEN" = t3."KD_DOSEN"
join "Matakuliah" t2 on t3."KD_MK" = t2."KD_MK"
group by t1."NM_DOSEN";
16.
select AVG("KAPASITAS")
from "Ruang";
17.
create view no17_1 as
select "NM_RUANG" ,count(t1."NIM") as "TOTALMAHASISWA"
from "Mahasiswa" t1
full join "KelasDetail" t2 on t1."NIM" = t2."NIM"
full join "Kelas" t3 on t2."KD_KELAS" = t3."KD_KELAS"
full join "Ruang" t4 on t3."KD_RUANG" = t4."KD_RUANG"
group by "NM_RUANG";

---
select * from No17_1
where "NM_RUANG" is null;

18.
select t1."NM_RUANG" , t1."KAPASITAS"
from "Ruang" t1
inner join (select min("KAPASITAS") as "MINN" from "Ruang") t2
on t1."KAPASITAS" =t2."MINN";
19.
select t1."NM_RUANG", t1"KAPASITAS"
from "Ruang" t1
inner join (Select max("KAPASITAS") as "MAXI" from "Ruang")
t2 on t1."KAPASITAS" ="MAXI";
20.
select "NM_DOSEN", "NM_MK", "NM_JURUSAN", "NM_FAKULTAS", "NM_KELAS", "NM_RUANG" ,"KAPASITAS"
from "Fakultas" t1
join "Jurusan" t2 on t1."KD_FAKULTAS" = t2."KD_FAKULTAS"
join "Matakuliah" t3 on t2."KD_JURUSAN" =t3."KD_JURUSAN"
join "Kelas" t4 on t3."KD_MK" =t4."KD_MK"
join "Ruang" t5 on t4."KD_RUANG" = t5."KD_RUANG"
join "Dosen" t6 on t4."KD_DOSEN"= t6."KD_DOSEN"
inner join (select max("KAPASITAS") as "BANYAK" from "Ruang")t7 on t5."KAPASITAS" = t7."BANYAK";
21.
select "NIM","NM_MAHASISWA", "NM_JURUSAN", "NM_FAKULTAS"
FROM "Fakultas" t1
join "Jurusan" t2 on t1."KD_FAKULTAS" = t2."KD_FAKULTAS"
join "Mahasiswa" t3 on t2."KD_JURUSAN" = t3."KD_JURUSAN";
22.
select "NM_FAKULTAS", "NM_JURUSAN", COUNT("NIM") AS "JML_MAHASISWA"
from "Fakultas" t1
join "Jurusan" t2 on t1."KD_FAKULTAS" = t2."KD_FAKULTAS"
join "Mahasiswa" t3 on t2."KD_JURUSAN" =t3."KD_JURUSAN"
group by t1."NM_FAKULTAS", t2."NM_JURUSAN";
23.
create view nomor31_1 as
select "NM_FAKULTAS", "NM_JURUSAN", count("NM_MAHASISWA") as "JML_MHS"
from "Fakultas" t1
join "Jurusan" t2 on t1."KD_FAKULTAS" = t2."KD_FAKULTAS"
join "Mahasiswa" t3 on t2."KD_JURUSAN" = t3."KD_JURUSAN";

---
select "NM_FAKULTAS","NM_JURUSAN","JML_MHS" from nomor31_1 t1
inner join (select max("JML_MHS") as "MAX" from nomor31_1)t2 on t1."JML_MHS" =t2."MAX"
24.
create view no24 as
select t1."NM_FAKULTAS", t2."NM_JURUSAN", count(t3."NM_MHS") as "JML_MHS"
from "Fakultas" t1
join "Jurusan" t2 on t1."KD_FAKULTAS" = t2."KD_FAKULTAS"
join "Mahasiswa" t3 on t2."KD_JURUSAN" = t3."KD_JURUSAN"
group by t1."NM_FAKULTAS", t2."NM_JURUSAN";

------

select "NM_FAKULTAS","NM_JURUSAN","JML_MHS" from no24 t1
inner join (select min("JML_MHS") as "MIN" from no24)t2 on t1."JML_MHS" =t2."MIN";
25.
select "NM_FAKULTAS", "NM_JURUSAN"
from "Fakultas" t1
join "Jurusan" t2 on t1."KD_FAKULTAS" = t2."KD_FAKULTAS"
full join "Mahasiswa" t3 on t2."KD_JURUSAN" = t3."KD_JURUSAN"
 where "NM_MAHASISWA" is null;
 26.  
select "ALAMAT", count("NIM") as "JML_MHS"
from "Mahasiswa"
group by "ALAMAT"
 27.
 select "JK", count ("NIM") as "JML_MHS"
from "Mahasiswa"
group by "JK";
28.
select "NM_MHS",count("NM_MHS") as "JML_MHS"
from "Mahasiswa"
where "NM_MHS" like '%Desi%'
group by "NM_MHS";
29.
select "NM_MHS", count("NM_MHS") as "JML_MHS"
from "Mahasiswa" 
where "NM_MHS" like '%Ratna%'
group by "NM_MHS";
30.
select t3."NIM", t3."NM_MHS", t2."NM_JURUSAN",t1."NM_FAKULTAS",t4."NM_MK",t4."SKS",t5."NILAI"
from "Fakultas" t1
join "Jurusan" t2 on t1."KD_FAKULTAS"= t2."KD_FAKULTAS"
join "Mahasiswa" t3 on t2."KD_JURUSAN"=t3."KD_JURUSAN"
join "Matakuliah" t4 on t2."KD_JURUSAN" = t4."KD_JURUSAN"
join "KelasDetail" t5 on t3."NIM" = t5."NIM"
31.
select t3."NIM", t3."NM_MHS", t2."NM_JURUSAN", t1."NM_FAKULTAS", t4."NM_MK", t4."SKS",t6."BOBOT"
from "Fakultas" t1
join "Jurusan" t2 on t1."KD_FAKULTAS" = t2."KD_FAKULTAS"
join "Mahasiswa" t3 on t2."KD_JURUSAN" = t3."KD_JURUSAN"
join "Matakuliah" t4 on t2."KD_JURUSAN" = t4."KD_JURUSAN"
join "KelasDetail" t5 on t3."NIM" = t5."NIM"
join "BobotNilai" t6 on t5."NILAI" = t6."KD_NILAI";
32.
select t3."NIM", t3."NM_MAHASISWA", t2."NM_JURUSAN", t1."NM_FAKULTAS", t6."NM_MK", sum( t6."SKS" * t7."BOBOT") as"BOBO_NILAI"
from "Fakultas" t1
join "Jurusan" t2 on t1."KD_FAKULTAS" = t2."KD_FAKULTAS"
join "Mahasiswa" t3 on t2."KD_JURUSAN" = t3."KD_JURUSAN"
join "KelasDetail" t4 on t3."NIM" = t4."NIM"
join "Kelas" t5 on t4."KD_KELAS" = t5."KD_KELAS"
join "Matakuliah" t6 on t5."KD_MK" = t6."KD_MK"
join "BobotNilai" t7 on t4."NILAI" = t7."KD_NILAI"
group by t3."NIM", t3."NM_MAHASISWA", t2."NM_JURUSAN", t1."NM_FAKULTAS", t6."NM_MK"

33.
select t3."NIM",t3."NM_MAHASISWA", t2."NM_JURUSAN",t1."NM_FAKULTAS", sum (t4."SKS"*t7."BOBOT")/sum(t4."SKS") as "IPK"
from "Fakultas" t1 
 join "Jurusan" t2 on t1."KD_FAKULTAS" = t2."KD_FAKULTAS"
join "Mahasiswa" t3 on t2."KD_JURUSAN" = t3."KD_JURUSAN"
join "Matakuliah" t4 on t2."KD_JURUSAN" = t4."KD_JURUSAN"
join "Kelas" t5 on t4."KD_MK" = t5."KD_MK"
join "KelasDetail" t6 on t5."KD_KELAS" = t6."KD_KELAS"
join "BobotNilai" t7 on t6."NILAI" = t7."KD_NILAI"
group by t3."NIM",t3."NM_MAHASISWA", t2."NM_JURUSAN",t1."NM_FAKULTAS"
34.
select t3."NIM", t3."NM_MAHASISWA", t2."NM_JURUSAN", t1."NM_FAKULTAS", t4."NM_MK",t4."SKS",t5."NILAI" ,t5."STATUS"
from "Fakultas" t1
join "Jurusan" t2 on t1."KD_FAKULTAS" = t2."KD_FAKULTAS"
join "Mahasiswa" t3 on t2."KD_JURUSAN" = t3."KD_JURUSAN"
join "Matakuliah" t4 on t2."KD_JURUSAN" = t4."KD_JURUSAN"
join "KelasDetail" t5 on t3."NIM" = t5."NIM"
where t5."STATUS" like '%LULUS%'
group by t3."NIM", t3."NM_MAHASISWA", t2."NM_JURUSAN", t1."NM_FAKULTAS", t4."NM_MK",t4."SKS",t5."NILAI",t5."STATUS"
35.
select t3."NIM", t3."NM_MAHASISWA", t2."NM_JURUSAN",t1."NM_FAKULTAS", t4."NM_MK",t4."SKS", t5."NILAI"
from "Fakultas"t1
join "Jurusan"t2 on t1."KD_FAKULTAS" =t2."KD_FAKULTAS"
join "Mahasiswa" t3 on t2."KD_JURUSAN" =t3."KD_JURUSAN"
join "Matakuliah" t4 on t2."KD_JURUSAN" = t4."KD_JURUSAN"
join "KelasDetail" t5 on t3."NIM" = t5."NIM"
where t5."STATUS" like '%MENGULANG%'
group by t3."NIM", t3."NM_MAHASISWA", t2."NM_JURUSAN",t1."NM_FAKULTAS", t4."NM_MK",t4."SKS", t5."NILAI"
