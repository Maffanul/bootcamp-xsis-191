package com.xsis.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.Tas;

@Controller
public class TasController {
	
	@RequestMapping("/tas/input")
	public String input() {
		return "tas/input";
	}
	
	@RequestMapping(value="/tas/save", method=RequestMethod.POST)
	public String save(@ModelAttribute Tas item, Model model) {
		model.addAttribute("data",item);
		return "tas/save";
	}
}
