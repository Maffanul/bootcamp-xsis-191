package com.xsis.demo.model;

public class Tas {

	private int id;
	private String nama;
	private String jenis;
	private String warna;
	private String bahan;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getJenis() {
		return jenis;
	}
	public void setJenis(String jenis) {
		this.jenis = jenis;
	}
	public String getWarna() {
		return warna;
	}
	public void setWarna(String warna) {
		this.warna = warna;
	}
	public String getBahan() {
		return bahan;
	}
	public void setBahan(String bahan) {
		this.bahan = bahan;
	}
	
	
}
