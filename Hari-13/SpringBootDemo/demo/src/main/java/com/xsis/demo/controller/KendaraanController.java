package com.xsis.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.Kendaraan;

@Controller
public class KendaraanController {

	@RequestMapping("/kendaraan/input")
	public String input() {
		return "kendaraan/input";
	}
	
	
	@RequestMapping(value="/kendaraan/save" , method=RequestMethod.POST)
	public String save(@ModelAttribute Kendaraan item, Model model) {
		model.addAttribute("data", item);
		return"kendaraan/save";
	}
}
