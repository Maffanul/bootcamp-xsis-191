package com.xsis.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.annotation.JacksonInject.Value;
import com.xsis.demo.model.Employee;
import com.xsis.demo.repository.EmployeeRepo;

@Controller
public class EmployeeController {
	@Autowired
	private EmployeeRepo repo;
	
	@RequestMapping("/employee/index")
	public String index(Model model) {
		List<Employee> data =repo.findAll();
		
		model.addAttribute("listData", data);
		
		return "employee/index";
	}
	
	@RequestMapping("/employee/add")
	public String add() {
		
		return "employee/add";
	}
	@RequestMapping(value="/employee/save", method=RequestMethod.POST)
	public String save(@ModelAttribute Employee item) {
		repo.save(item);
		
		return"redirect:/employee/index";
	}
	
}
