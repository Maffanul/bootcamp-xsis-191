package com.xsis.demo.model;

public class Kendaraan {
	private int id;
	private String nama;
	private String warna;
	private String jenis;
	private String nopol;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getWarna() {
		return warna;
	}
	public void setWarna(String warna) {
		this.warna = warna;
	}
	public String getJenis() {
		return jenis;
	}
	public void setJenis(String jenis) {
		this.jenis = jenis;
	}
	public String getNopol() {
		return nopol;
	}
	public void setNopol(String nopol) {
		this.nopol = nopol;
	}
	
	

}
