package com.xsis.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.Biodata;
import com.xsis.demo.repository.BiodataRepo;

@Controller
public class BiodataController {
	//membuat auto instance dari repository(action database dengan model)
	@Autowired
	private BiodataRepo repo;
	
	
	//request yang ada du url localhost:port/biodata/index
	@RequestMapping("/biodata/index")
	public String index(Model model) {
		//membuat object list biodata, kemudian diisi dari 
		//object repo dengan method findall()
		List<Biodata> data =repo.findAll();
		//mengirim variabel listData, valuenya disi dari object data 
		model.addAttribute("listData", data);
		// menampilkan view src/main/resources/templates
		return "biodata/index";
	}
	//requeas yang ada di localhost:port/bidata/add	
	@RequestMapping("/biodata/add")
	public String add() {
		//menampilkan view src/main/resource/templates
		return "biodata/add";
	}
	
	//reques yang ada di url localhost:port/biodata/save
	//method nya ada post
	@RequestMapping(value="/biodata/save", method=RequestMethod.POST)	
	public String save(@ModelAttribute Biodata item) {
		//simpan ke database
		repo.save(item);
		//redirect : akan di teruskan ke halaman index
		return"redirect:/biodata/index";
	}
	//request edit data
	@RequestMapping(value="/biodata/edit/{id}")
	public String edit(Model model, @PathVariable(name="id") Integer id) {
		Biodata item = repo.findById(id).orElse(null);
		model.addAttribute("data",item);
		return "biodata/edit";
	}
	
	//eques delete data
	@RequestMapping(value="/biodata/delete/{id}")
	public String hapus(@PathVariable(name="id")Integer id) {
		// mengambil data dari data base dengan parameter id
		Biodata item = repo.findById(id).orElse(null);
		//remove database
		if(item!=null) {
			repo.delete(item);
		}
		return "redirect:/biodata/index";
	}
}
