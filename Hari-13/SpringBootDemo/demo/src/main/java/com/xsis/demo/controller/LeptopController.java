package com.xsis.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.Leptop;

@Controller
public class LeptopController {

	@RequestMapping("/leptop/add")
	public String add() {
		return "leptop/add";
	}
	
	
	@RequestMapping(value="/leptop/save", method=RequestMethod.POST)
	public String save(@ModelAttribute Leptop item, Model model) {
		model.addAttribute("data", item);
		return "leptop/save";
	}
}
